import re

def represent_float(s):
    try: 
        float(s)
        return True
    except ValueError:
        return False


class Calc:
    
    def add(self, strg, delim_pattern=','):
        if strg == '':
            return 0
        if represent_float(strg):
            if float(strg) < 0:
                raise ValueError
            if float(strg) > 1000:
                return 0
            
            return float(strg)
        
        if strg[0] == '/' and strg[1] == '/':
            delim_pattern += '|' + strg[2]
        
        sum_of_input = 0
        for split in re.split(delim_pattern, strg):
            value = float(split)
            if value > 1000:
                value = 0
            sum_of_input += value
        return sum_of_input
        
class CalcTest:
    def __init__(self):
        self.calc = Calc()
    
    def test_empty_string(self):
        assert self.calc.add("") == 0
        
    def test_single_number(self):
        assert self.calc.add("420") == 420
        
    def test_comma_delimited_sum(self):
        assert self.calc.add("3,3") == 6
        
    def test_newline_delimited_sum(self):
        assert self.calc.add("3\n3", '\n') == 6
        
    def test_multiple_delimited_sum(self):
        assert self.calc.add("3\n3,3", ',|\\n') == 9
        
    def test_negative_throws(self):
        try:
            self.calc.add("-3") 
        except ValueError:
            assert True
            return
        assert False
            
    def test_ignore_larger_than_1000(self):
        assert self.calc.add("1001", ',|\\n') == 0
        assert self.calc.add("1000,1", ',|\\n') == 1

    def test_str_defined_delimiter(self):
        assert self.calc.add("//#3#3,1") == 7
                             
    def test_multichar_delimiter(self):
        assert self.calc.add("//[add]3add3,1") == 7
        
    def test_multiple_multichar_delimiter(self):
        assert self.calc.add("//[add][#]3add3#1") == 7


tester = CalcTest()
tester.test_empty_string()
tester.test_single_number()
tester.test_comma_delimited_sum()
tester.test_newline_delimited_sum()
tester.test_multiple_delimited_sum()